# Reto Número 1
El reto número 1 está compuesto por 2 preguntas:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

## Metodología
La construcción de la imagen solicitada se basó en el uso de buenas prácticas, documentadas por el proyecto de Docker[1]
## Software requerido
Todo el proyecto se desarrolló sobre Ubuntu Bionic (Ubuntu 18.04.6 LTS) y se instalaron los siguientes paquetes:
* jq
* git
* docker
* node

Se proporciona un archivo instala.sh para facilitar el proceso de adecuación de la plataforma donde vaya a reproducirse este repositorio.

## Acerca de la imagen
La imagen se construye con el comando

```sh
sudo docker build  -t hectorcolina/imagen-reto-1 .
```
Donde
| PARÁMETRO  | FUNCIÓN |
| ---------- | ------- |
| build      | Indica a docker que construya imagen a partir de archivo Dockerfile presente en el directorio donde se invoque el comando |
| -t         | Tag para personalizar identificador de la imagen|



Y, la imagen, se hace funcionar con el comando

```sh
sudo docker run --name reto1 -p 3000:3000 -d hectorcolina/imagen-reto-1
```
Donde
| PARÁMETRO  | FUNCIÓN |
| ---------- | ------- |
| run      | Indica a docker que ejecute la imnagen |
| - -name   | Identificador que se dará al contenedor|
| - -p      | Puerto que se expondra desde el contenedor al sistema anfitrión|
| - -d      | Comienza el contenedor en forma "detached"|

## Evidencias
Se construyó una imagen basada en Alpine [2]. La imagen final sólo ocupa 137 Mb
![Imagen Docker personalizada basada en Alpine](img/custom_image.png)

Luego, se hicieron las consultas a la api de la aplicación que se está ejecutando en el contenedor obteniendo el siguiente resultado
![Consultas a la API](img/api_custom.png)

## Referencias
[1] https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

[2] https://hub.docker.com/_/alpine


