#!/bin/bash
# Script for install required dependencies used by this approach
# By Hector Colina
# V 0.1
# 04-04-2022

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
    else 
	    echo "You're root so we will be installing required software"
	    echo  "BE ADVICE: THIS SCRIPT ONLY MUST BE RUN IF YOU'RE USING UBUNTU 18"
	    read -n 1 -s -r -p "Press any key to continue if you're using Ubuntu 18, otherwhise press CTRL+C"
	    apt update
	    apt install -y git jq apt-transport-https ca-certificates curl software-properties-common
	    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
	    sudo apt update
	    apt install -y docker-ce
	    cd ~
	    curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
	    bash nodesource_setup.sh
	    apt install -y nodejs
fi


