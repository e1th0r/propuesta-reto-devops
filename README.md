# Reto-DevOps CLM
![CLM Consultores](./img/clm.png)
## Propuestas de Hector Colina
En el siguiente repositorio se presentan las aproximaciones al reto DevOps. Estas aproximaciones son desarrolladas por Hector Colina.
La organización de este repositorio es la siguiente:
### Organización del repositorio
 * [README.md](./README.md)
 * [reto1](./reto1)
   * [Dockerfile](./reto1/Dockerfile)
   * [index.js](./reto1/index.js)
   * [package.json](./reto1/package.json)
   * [package-lock.json](./reto1/package-lock.json)
   * [instala.sh](./reto1/instala.sh)
   * [README.md](./reto1/README.md)

## Respuesta Reto 1
Nuestra propuesta para el reto número 1 puede ser conseguida en el directorio [reto1](./reto1)

